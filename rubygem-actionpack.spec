%global gem_name actionpack
%{?_with_bootstrap: %global bootstrap 1}
%global bootstrap 1
Name:                rubygem-%{gem_name}
Epoch:               1
Version:             5.2.4.4
Release:             3
Summary:             Web-flow and rendering framework putting the VC in MVC (part of Rails)
License:             MIT
URL:                 http://rubyonrails.org
Source0:             https://rubygems.org/gems/%{gem_name}-%{version}.gem
Source1:             https://github.com/rails/rails/archive/v5.2.4.4.tar.gz
Patch0:              CVE-2021-22885.patch
Patch1:              CVE-2021-22904.patch

BuildRequires:       ruby(release) rubygems-devel ruby >= 2.2.2
%if ! 0%{?bootstrap}
BuildRequires:       rubygem(activemodel) = %{version} rubygem(activerecord) = %{version}
BuildRequires:       rubygem(activesupport) = %{version} rubygem(actionview) = %{version}
BuildRequires:       rubygem(railties) = %{version} rubygem(rack) rubygem(rack-cache)
BuildRequires:       rubygem(rack-test) rubygem(puma) rubygem(capybara) >= 2.13.0
%endif
BuildArch:           noarch
%description
Eases web-request routing, handling, and response as a half-way front,
half-way page controller. Implemented with specific emphasis on enabling easy
unit/integration testing that doesn't require a browser.

%package doc
Summary:             Documentation for %{name}
Requires:            %{name} = %{epoch}:%{version}-%{release}
BuildArch:           noarch
%description doc
Documentation for %{name}.

%prep
%setup -q -c -T
%gem_install -n %{SOURCE0}
%patch0 -p1
%patch1 -p1

%build

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/
%if ! 0%{?bootstrap}

%check
pushd .%{gem_instdir}
tar xzvf %{SOURCE1}
cd rails-%{version}/%{gem_name}
ruby -Ilib:test -e 'Dir.glob "./test/**/*_test.rb", &method(:require)'
popd
%endif

%files
%dir %{gem_instdir}
%license %{gem_instdir}/MIT-LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/README.rdoc

%changelog
* Mon Jun 28 2021 wangyue<wangyue92@huawei.com> - 5.2.4.4-3
- Fix CVE-2021-22904

* Fri Jun 11 2021 wangyue<wangyue92@huawei.com> - 5.2.4.4-2
- Fix CVE-2021-22885

* Mon Feb  8 2021 sunguoshuai<sunguoshuai@huawei.com>- 5.2.4.4-1
- Upgrade to 5.2.4.4

* Sat Aug 8 2020 chengzihan <chengzihan2@huawei.com> - 5.2.3-1
- Package init
